#!/usr/bin/env bash
#
# - strings
#
# library containing various functions to mess around with strings
#
# much of what is here is taken from various sources on the internet
#

# cut $1 from start of string $2
cut_start() {
  printf '%s\n' "${2##$1}"
}

# cut $1 from end of string $2
cut_end() {
  printf '%s\n' "${2%%$1}"
}

# remove all trailing and leading whitespace
rm_ws() {
  trim=${1#${1%%[![:space:]]*}}
  trim=${trim%${trim##*[![:space:]]}}
  printf '%s\n' "$trim"
}

# like above but also truncates spaces to 1
# shellcheck disable=SC2086,SC2048
rm_ws_tc() {
  set -f
  set -- $*
  printf '%s\n' "$*"
  set +f
}

# remove all quotes from string
rm_quotes() {
  set -f
  old_ifs=$IFS
  IFS=\"\'
  # shellcheck disable=2086
  set -- $1
  IFS=
  printf '%s\n' "$*"
  IFS=$old_ifs
  set +f
}

# change $1 to lowercase
lowercase() {
  printf '%s\n' "${1,,}"
}

# change $1 to uppercase
upper() {
  printf '%s\n' "${1^^}"
}

# cut all instances of $1 from $2
cut_pattern() {
  printf '%s\n' "${2//$1/}"
}

# cut first instance of $1 from $2
cut_first() {
  printf '%s\n' "${2/$1/}"
}

# percent-encode a string
urlencode() {
  local LC_ALL=C
  for ((i = 0; i < ${#1}; i++)); do
    : "${1:i:1}"
    case "$_" in
    [a-zA-Z0-9.~_-])
      printf '%s' "$_"
      ;;

    *)
      printf '%%%02X' "'$_"
      ;;
    esac
  done
  printf '\n'
}

# decode a percent-encoded string
urldecode() {
  : "${1//+/ }"
  printf '%b\n' "${_//%/\\x}"
}
