" inbuilt vim macro
runtime macros/matchit.vim

" toggle line numbers and cursorline
nnoremap <C-x> :set nonumber! norelativenumber!<CR>
nnoremap <C-a> :set cursorline!<CR>

" split navigation
nnoremap <C-H> <C-W><C-H>
nnoremap <C-L> <C-W><C-L>

" better line nav
nnoremap k gk
nnoremap j gj
nnoremap H g0
nnoremap L g$
vnoremap k gk
vnoremap j gj
vnoremap H g0
vnoremap L g$

" ezformat
nnoremap Q gwip
inoremap <C-q> <Esc>gwip i

" move lines
nnoremap <C-j> :m .+1<CR>==
nnoremap <C-k> :m .-2<CR>==
inoremap <C-j> <Esc>:m .+1<CR>==gi
inoremap <C-k> <Esc>:m .-2<CR>==gi

" correct typos
inoremap <C-l> <c-g>u<Esc>[s1z=`]a<c-g>u

" readline??
inoremap <C-e> <esc>A
inoremap <C-a> <esc>I

" leave
nnoremap <C-q> ZZ
