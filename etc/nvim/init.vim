" tmux
if &term =~ '256color'
    set t_ut=

endif

" save eyes
set signcolumn=yes

" bahahahaaha
set scrolloff=10

" title
set title
set titlestring=%f

" modeline
set modeline
set modelines=5

" live life on the edge
set nowritebackup
set nobackup
set noswapfile

" color stuff
let &t_8f = "\<Esc>[41;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
set background=dark
set t_Co=254
syntax on
colorscheme mnml

" EOB fix
set fillchars=fold:\ ,vert:\│,eob:\ ,msgsep:‾

" look and feel
set ruler
" set number
" set cursorline
set laststatus=2
set noshowmode
set linebreak
set hidden

" good splitting
set splitbelow
set splitright

" tab settings
set tabstop=2
set softtabstop=2
set shiftwidth=2
set nosmartindent
set nocopyindent
set backspace=indent,eol,start
filetype plugin indent on

" noshowcmd
set noshowcmd

" folding
set foldmethod=manual
set foldnestmax=3
set foldenable

" misc
set wildmenu
set lazyredraw
set autoread

" search
set incsearch
set hlsearch
set ignorecase
let g:rehash256=1

" shell
set history=1000
set shell=zsh

" show whitespaces as red
hi!  ExtraWhitespace ctermbg=red guibg=red
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call  clearmatches()

" spellcheck only for certain filetypes
autocmd FileType markdown setlocal spell spelllang=en_us
autocmd FileType html setlocal spell spelllang=en_us
autocmd FileType tex setlocal spell spelllang=en_us

" conceals
autocmd FileType markdown setlocal conceallevel=2

" find highlight group ez
function! SynStack ()
    for i1 in synstack(line("."), col("."))
        let i2 = synIDtrans(i1)
        let n1 = synIDattr(i1, "name")
        let n2 = synIDattr(i2, "name")
        echo n1 "->" n2
    endfor
endfunction
nnoremap gm :call SynStack()<CR>

" sources
source ~/etc/nvim/statusline.vim
source ~/etc/nvim/plugins.vim
source ~/etc/nvim/keybinds.vim
