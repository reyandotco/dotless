" fix some filetypes
autocmd BufNewFile,BufRead *.md set filetype=markdown
autocmd BufNewFile,BufRead *.tex set filetype=tex
autocmd BufNewFile,BufRead *.cls set filetype=tex
autocmd BufNewFile,BufRead *.sty set filetype=tex
autocmd BufNewFile,BufRead *.sch set filetype=sch
autocmd FileType md,markdown ALEDisable

" vim-plugins
call plug#begin('~/etc/nvim/plugged')

Plug 'kyazdani42/nvim-tree.lua'
Plug 'kyazdani42/nvim-web-devicons'
    nnoremap <leader>tt :NvimTreeToggle<CR>
    let g:nvim_tree_ignore = [ '.git', '.gitignore']
    let g:nvim_tree_root_folder_modifier = ':et'
    let g:nvim_tree_highlight_opened_files = 1
    let g:nvim_tree_icon_padding = '  '
    let g:nvim_tree_indent_markers = 1
    let g:nvim_tree_hide_dotfiles = 1
    let g:nvim_tree_add_trailing = 1
    let g:nvim_tree_auto_close = 1
    let g:nvim_tree_auto_open = 1
    let g:nvim_tree_width = 30
    let g:nvim_tree_show_icons = {
    \ 'folder_arrows': 1,
    \ 'folders': 1,
    \ 'files': 0,
    \ 'git': 0,
    \ }

Plug 'dense-analysis/ale'
    nnoremap <C-g> :ALEGoToDefinition<CR>
    let g:ale_sh_shellcheck_options = ''
    let g:ale_sh_shfmt_options = '-i 0 -ci -kp -p'
    let g:ale_lint_on_text_changed = 'never'
    let g:ale_lint_on_insert_leave = 0
    let g:ale_fix_on_save = 1
    let g:ale_sign_error = '~'
    let g:ale_sign_warning = '-'
    let g:ale_sign_info= '^'
    let g:ale_linters = {
        \   'cpp':  ['cc', 'clangd'],
        \   'c'  :  ['cc', 'clangd'],
        \   '*sh':  ['shellcheck'],
        \   'java': ['javac'],
        \}
    let g:ale_fixers = {
        \   '*':          ['remove_trailing_lines', 'trim_whitespace'],
        \   'tex':        ['latexindent', 'trim_whitespace'],
        \   'cpp':        ['clang-format', 'clangtidy'],
        \   'c':          ['clang-format', 'clangtidy'],
        \   'java':       ['google_java_format'],
        \   'html':       ['prettier'],
        \   'javascript': ['prettier'],
        \   'css':        ['prettier'],
        \   'sh':         ['shfmt'],
        \}

Plug 'lervag/vimtex'
    autocmd FileType tex,plaintex call vimtex#init()
    nnoremap <leader>ss <cmd>:VimtexTocOpen<CR>
    let g:tex_flavor='latex'
    let g:vimtex_view_method='zathura'
    let g:vimtex_quickfix_mode=0
    let g:tex_conceal= 'bs'
    let g:tex_fast = "bMpr"

Plug 'neoclide/coc.nvim', {'branch': 'release'}
    inoremap <expr> <Tab> pumvisible() ? "\<C-n>"
    inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>"
    function! s:check_back_space() abort
        let col = col('.') - 1
        return !col || getline('.')[col - 1]  =~ '\s'
    endfunction

Plug 'SirVer/ultisnips'
    let g:UltiSnipsExpandTrigger="<tab>"
    let g:UltiSnipsJumpForwardTrigger="<tab>"
    let g:UltiSnipsJumpBackwardTrigger="<S-tab>"

Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
    nnoremap <leader>ff :Files<CR>
    nnoremap <leader>dd :Buffers<CR>

Plug 'rhysd/clever-f.vim'
    let g:clever_f_across_no_line = 1
    let g:clever_f_mark_direct = 1

Plug 'AndrewRadev/tagalong.vim'
    inoremap <silent> <c-c> <c-c>:call tagalong#Apply()<cr>

Plug 'godlygeek/tabular'
Plug 'tpope/vim-endwise'
Plug 'junegunn/goyo.vim'
Plug 'tpope/vim-surround'
Plug 'jiangmiao/auto-pairs'
Plug 'tpope/vim-commentary'
Plug 'sheerun/vim-polyglot'

call plug#end()
